import React from "react";
import './Task.css';
import deleteIcon from '../delete.png';

const Task = props => {
    return (
        <div className='Task'>
            <p>{props.task}</p>
            <button onClick={props.onRemove} className='deleteButton'><img src={deleteIcon} alt='deleteIcon' width='30px'/></button>
        </div>
    )
}

export default Task;