import React from "react";
import './AddTaskForm.css';

const AddTaskForm = props => {
    return (
        <div className='TaskForm'>
            <input className='form'
                type='text'
                value={props.val}
                onChange={props.onInputChange}
            />
            <button onClick={props.addTask} className='formButton'>Add</button>
        </div>
    )
}

export default AddTaskForm;