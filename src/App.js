import {useState} from "react";
import {nanoid} from "nanoid";
import Task from "./Task/Task";
import AddTaskForm from "./Task/AddTaskForm";

const App = () => {
    const [tasks, setTasks] = useState([
        {task: 'Clean the house', id: 1},
        {task: 'Do homework', id: 2},
        {task: 'Go to walk', id: 3},
    ]);

    const [val, setVal] = useState('');

    const onInputChange = (e) => {
        setVal(e.target.value);
    }

    const removeTask = id => {
        setTasks(tasks.filter(t => t.id !== id));
    };



    const addTask = () => {
        if(val) {
            setTasks([...tasks, {
                task: val,
                id: nanoid()
            }]);
            setVal('');
        }
    }

    const tasksComponents = tasks.map(el => (
        <Task
            key={el.id}
            id={el.id}
            task={el.task}
            onRemove={() => removeTask(el.id)}
        >
        </Task>
    ));

    return (
        <div>
            <h1>Add Task</h1>
            <AddTaskForm
                onInputChange={onInputChange}
                val={val}
                addTask={addTask}
            />
            {tasksComponents}
        </div>
    )

}

export default App;
